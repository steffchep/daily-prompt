'use strict';

export function SelectableItem (name, isSelected = true) {
	return {
		name,
		isSelected,
	};
}

export function mergeStored(items, storedItems) {
	const results = []
	for (const item of items) {
		const stored = findbyName(storedItems, item.name);
		const isSelected = stored ? stored.isSelected : item.isSelected;
		results.push({...item, isSelected});
	}
	return results;
}

function findbyName(items, name) {
	return items ? items.find((item) => item.name === name) : null;
}
