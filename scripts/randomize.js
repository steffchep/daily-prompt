'use strict';

export function randomFromArray(array) {
	if (array.length === 0) { return null; }
	if (array.length === 1) { return array[0]; }

	return array[randomNumber(array.length)];
}

const randomNumber = (max) => Math.floor(Math.random() * max);
