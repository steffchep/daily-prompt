'use strict';

import styles from './list-item-styles.js';

const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
</style>
<div class="list-item">
	<label><input type="checkbox" id="the-checkbox"><span class="is-sr-only">include</span><span id="the-prompt"></span></label>
</div>
`;

export class ListItem extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));
		shadowRoot.querySelector('style').innerHTML = styles;
		this._promptElement = shadowRoot.querySelector('#the-prompt');
		this._checkboxElement = shadowRoot.querySelector('#the-checkbox');
		this._checkboxElement.addEventListener('change', (e) => {
			this.checked = e.currentTarget.checked;
		});
	}

	connectedCallback() {
		this.prompt = this.getAttribute('prompt');
		this.checked = this.hasAttribute('checked')
	}

	set prompt(value) {
		this._prompt = sanitize(value);
		this.setAttribute('prompt', this._prompt);
		this._promptElement.innerHTML = this._prompt;
	}

	get prompt() {
		return this._prompt;
	}

	set disabled(value) {
		if (value) {
			this.setAttribute('disabled', '');
		} else {
			this.removeAttribute('disabled');
		}
		this._checkboxElement.disabled = value;
	}

	get disabled() {
		return this.hasAttribute('disabled');
	}

	set checked(value) {
		const dispatchEvt = this.checked != value;

		if (value) {
			this.setAttribute('checked', '');
		} else {
			this.removeAttribute('checked');
		}
		this._checkboxElement.checked = value;

		if (dispatchEvt) {
			this.dispatchEvent(new Event('change'));
		}
	}

	get checked() {
		return this.hasAttribute('checked');
	}

	set showcase(value) {
		if (value) {
			this.setAttribute('showcase', '');
		} else {
			this.removeAttribute('showcase');
		}
	}

	get showcase() {
		return this.hasAttribute('showcase');
	}

}

function sanitize(htmlString) {
	return htmlString.replaceAll('<br>', '\n').replaceAll('<', '&lt').replaceAll('\n', '<br>');
}