'use strict';

import { getMillisFromCssProp } from './sp-dialog.js';

const confirmationTemplate = document.createElement('template');
confirmationTemplate.innerHTML = `
	<sp-dialog modal>
		<section aria-labelledby="confirmation-heading" class="dialog-content">
			<h2 id="confirmation-heading"></h2>
			<div id="help-text"></div>
			<div id="confirmation-text">Are you sure?</div>
			<div class="buttons">
				<button id="confirm-yes" class="is-primary" type="submit">Yes</button>
				<button id="confirm-no" type="button">No</button>
			</div>		
		</section>
	</sp-dialog>
`;

const alertTemplate = document.createElement('template');
alertTemplate.innerHTML = `
	<sp-dialog modal>
		<section aria-labelledby="confirmation-heading" class="dialog-content">
			<h2 id="confirmation-heading"></h2>
			<div class="buttons">
				<button id="confirm-yes" class="is-primary" type="submit">Ok</button>
			</div>		
		</section>
	</sp-dialog>
`;

function Confirmation(vsModalElem) {
	const headingElem = vsModalElem.querySelector('#confirmation-heading');
	const helpTextElem = vsModalElem.querySelector('#confirmation-help-text');
	const yesButton = vsModalElem.querySelector('#confirm-yes');
	const noButton = vsModalElem.querySelector('#confirm-no');

	function _confirmYes(e) {
		e.stopPropagation();
		vsModalElem.dispatchEvent(new Event('confirm-yes'));
	}
	
	function _confirmNo(e) {
		e.stopPropagation();
		vsModalElem.dispatchEvent(new Event('confirm-no'));
	}
	
	function setHeading(heading) {
		headingElem.textContent = heading;
	}

	function setHelpText(heading) {
		helpTextElem.textContent = heading;
	}

	yesButton.addEventListener('click', _confirmYes);
	noButton?.addEventListener('click', _confirmNo);

	return {
		setHeading,
		setHelpText,
	}
};

export function getConfirmation(text, confirmYes, confirmNo = () => {}, alertOnly = false) {
	const confirmationDiv = document.createElement('div');
	confirmationDiv.appendChild((alertOnly ? alertTemplate : confirmationTemplate).content.cloneNode(true));

	document.body.appendChild(confirmationDiv);

	const confirmationElem = confirmationDiv.querySelector('sp-dialog');
	const confirmation = Confirmation(confirmationElem);

	setTimeout(() => {
		confirmation.setHeading(text);
		confirmationElem.open = true;
		// apparently you can tab out of the modal dialog after all. If focus is not set by hand, the keyup handler oft eh component won't get caught. (might need to hook into the native dialog's close to get it animating all the time)
		confirmationElem.querySelector('#confirm-yes').focus();
	}, 0);

	const transitionDuration = getMillisFromCssProp(
		getComputedStyle(confirmationElem).getPropertyValue('--modal-interactive-transition-duration')
	);

	function closeAndRemove() {
		confirmationElem.open = false;
		setTimeout(() => {
			document.body.removeChild(confirmationDiv);
		}, transitionDuration + 50);
	}

	confirmationElem.addEventListener('confirm-yes', (e) => { 
		confirmYes(e);
		closeAndRemove();
	});

	confirmationElem.addEventListener('confirm-no', (e) => { 
		confirmNo(e);
		closeAndRemove();
	});

}

export function showAlert(text, modalClosed = () => {}) {
	getConfirmation(text, modalClosed, () => {}, true);
} 