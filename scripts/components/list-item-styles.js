'use strict';

export default /*css*/`
:host {
	display: block;
	box-sizing: border-box;
	text-align: left;
	max-width:  var(--sp-list-item-width);
	border: 1px solid var(--sp-color-border);
	border-radius: var(--sp-spacing-radius);
	background-color: transparent;
}
:host(:not([checked])) {
	box-shadow: none;
	background-color: transparent;
}
:host(:not([checked])) .tile img {
	filter: saturate(50%) contrast(15%) brightness(175%);
}
:host(:not([disabled]):hover), :host(:not([disabled]):focus-within) {
	cursor: pointer;
}
:host(:not([disabled]):hover) .list-item, :host(:not([disabled]):focus-within) .list-item {
	border-color: var(--sp-color-interactive);
}
:host(:not([disabled])[checked]) {
	background-color: var(--sp-color-interactive-background-muted);
}
:host(:not([disabled]):focus-within) {
	outline: 2px solid var(--sp-color-interactive-outline);
	outline-offset: 2px;
}

:host([showcase]) {
	--sp-list-item-width: 450px;
	min-height: calc(var(--sp-list-item-width) / 3);
	display: grid;
	place-items: center;
	margin: auto;
	font-weight: bold;
	font-size: larger;
	border: 5px solid var(--sp-color-interactive);
	text-align: center;
}

:host([showcase]) label {
	padding: var(--sp-spacing-4x);
}

:host([showcase]) .list-item {
	border-left-width: var(--sp-spacing-2x);
}

:host([showcase][disabled]) label {
	grid-template-columns: 1fr;
}

:host([showcase][disabled]) input {
	display: none;
}

.list-item {
	min-height: inherit;
	border-radius: var(--sp-spacing-radius);
	border-left: var(--sp-spacing-1x) solid transparent;
	width: 100%;
}

.is-sr-only {
	position: absolute;
	display: block;
	width: 1px;
	height: 1px;
	top: -999999px;
}

label {
	padding: var(--sp-spacing-2x) var(--sp-spacing-3x) var(--sp-spacing-2x) var(--sp-spacing-2x);
	cursor: inherit;
	display: grid;
	text-align: inherit;
	grid-template-columns: min-content 1fr;
	align-items: center;
	gap: 0.5em;
	min-height: inherit;
}

input {
	cursor: inherit;
	accent-color: var(--sp-color-interactive);
}

input:focus {	
	outline: 2px solid var(--sp-color-interactive-outline);
}
`;