'use strict';

const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
	:host {
		cursor: pointer;
		box-sizing: border-box;
		display: inline-block;
		padding: 0 var(--sp-spacing-1x);
		width: auto;
		text-align: left;
		border: 1px solid transparent;
		border-radius: var(--sp-spacing-radius);
	}
	:host(:hover) {
		background-color: var(--sp-color-main-background-muted);
		border-color: var(--sp-color-border);
	}
	:host(:focus-within) {	
		background-color: var(--sp-color-main-background-muted);
		outline: 2px solid var(--sp-color-interactive-outline);
		outline-offset: -2px;
	}
	input, label {
		cursor: inherit;
		display: inline-block;
		padding: 0.25em;
	}
	input {
		accent-color: var(--sp-color-interactive);
	}
	input:focus {	
		outline: none;
	}
</style>
<input type="checkbox" id="the-checkbox"><label for="the-checkbox"></label>
`;

export class CheckboxWithLabel extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));
		this._inputElement = shadowRoot.querySelector('input');
		this._labelElement = shadowRoot.querySelector('label');
		this._inputElement.addEventListener('change', (e) => {
			this.checked = e.currentTarget.checked;
		});
	}

	connectedCallback() {
		this.checked = this.hasAttribute('checked')
		this._labelElement.innerHTML = this.getAttribute('label');
	}
	get checked() {
		return this.hasAttribute('checked');
	}
	set checked(value) {
		const dispatchEvt = this.checked != value;

		if (value) {
			this.setAttribute('checked', '');
		} else {
			this.removeAttribute('checked');
		}
		this._inputElement.checked = value;

		if (dispatchEvt) {
			this.dispatchEvent(new Event('change'));
		}
	}

	get label() {
		return this.getAttribute('label');
	}
	set label(value) {
		this.setAttribute('label', value);
		this._labelElement.innerHTML = value;
	}
}
