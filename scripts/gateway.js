'use strict';

const baseGetUrl = "/";
const postUrl = "/json.php?";

export function getToken() {
	const parts = window.location.href.split('?');
	if (parts.length == 2) {
		return parts[1];
	}
	return '';
}

export async function save(json, tokenSuffix = '') {
	const queryString = getToken() + tokenSuffix;
	if (!queryString) { return; };
	console.log('Write to server...', json);
	await writeJson(json, queryString);
}

export async function load(tokenSuffix = '') { 
	const queryString = getToken() + tokenSuffix;
	if (!queryString) { return; };
	console.log('Loading')
	return fetchJson(queryString + '.json');
}

const postResultHandler = async (result, message) => {
	if (result.status === 200) {
		console.log(message);
	} else {
		throw `Request failed: ${result.status} - ${result.statusText}`;
	}
}

async function fetchJson(relativePath) {
	console.log('getting data from server...');
	return fetch(baseGetUrl + relativePath)
			.then((response) => {
				postResultHandler(response, 'Loaded');
				return response.json();
			})
			.then((json) => json);
};


const writeJson = (data, queryString = '') => {
	const postOptions = {
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		method: "POST",
		body: JSON.stringify(data, null, ' ')
	}
	fetch(postUrl + queryString, postOptions)
		.then((result) => postResultHandler(result, 'Saved to server'));
};
