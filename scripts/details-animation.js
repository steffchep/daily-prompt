export class DetailsAnimation {
	constructor(el, invokerSelector = 'summary', contentSelector = '.details-content') {
		this.el = el;
		this.summary = el.querySelector(invokerSelector);
		this.content = el.querySelector(contentSelector);

		this.animation = null;
		this.isClosing = false;
		this.isExpanding = false;
		this.closedHeight = `${this.summary.offsetHeight}px`;
		this.summary.addEventListener('click', (e) => this.onClick(e));
	}

	onClick(e) {
		e.preventDefault();
		this.el.style.overflow = 'hidden';
		if (this.isClosing || !this.el.open) {
			this.grow();
		} else if (this.isExpanding || this.el.open) {
			this.shrink();
		}
	}

	shrink() {
		this.isClosing = true;
		this.el.setAttribute('closing', '');

		const startHeight = `${this.el.offsetHeight}px`;
		const endHeight = this.closedHeight;

		const {
			easingFunction,
			transitionDuration,
		} = getAnimationProperties(this.el);

		if (this.animation) {
			this.animation.cancel();
		}

		this.animation = this.el.animate({
			height: [startHeight, endHeight],
		},
		{
			duration: transitionDuration,
			easing: easingFunction,
		});

		this.animation.onfinish = () => this.onAnimationFinish(false);
		this.animation.oncancel = () => this.isClosing = false;
	}

	grow() {
		this.el.style.height = `${this.el.offsetHeight}px`;
		this.el.open = true;
		window.requestAnimationFrame(() => this.expand());
	}

	expand() {
		this.isExpanding = true;
		const startHeight = this.closedHeight;
		const endHeight = `${this.summary.offsetHeight + this.content.offsetHeight}px`;

		const {
			easingFunction,
			transitionDuration,
		} = getAnimationProperties(this.el);

		if (this.animation) {
			this.animation.cancel();
		}

		this.animation = this.el.animate({
			height: [startHeight, endHeight],
		},
		{
			duration: transitionDuration,
			easing: easingFunction,
		});
		
		this.animation.onfinish = () => this.onAnimationFinish(true);
		this.animation.oncancel = () => this.isExpanding = false;
	}

	onAnimationFinish(open) {
		this.el.open = open;
		this.animation = null;
		this.isClosing = false;
		this.isExpanding = false;
		this.el.style.height = this.el.style.overflow = '';
		this.el.removeAttribute('closing');
		this.content.inert = !open;
	}
}

function getAnimationProperties(elem) {
	let easingFunction = getComputedStyle(elem).getPropertyValue('--sp-interactive-transition-function'	);
	if (!easingFunction) {
		easingFunction = 'ease';
	}
	if (easingFunction === 'none') {
		easingFunction = null;
	}
	return {
		easingFunction,
		transitionDuration: getMillisFromCssProp(getComputedStyle(elem).getPropertyValue('--sp-interactive-transition-duration')),
	};
}

function getMillisFromCssProp(cssProp) {
	try {
		if (cssProp.includes('ms')) {
			return Number(cssProp.replace('ms', ''));
		} else if (cssProp.includes('s')) {
			const seconds = Number(cssProp.replace('s', ''));
			return seconds * 1000;
		} else {
			return 0;
		}
	} catch (error) {
		console.log({ error });
		return 500;
	}
}
