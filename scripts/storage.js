'use strict';

import { getToken, load, save } from './gateway.js';

const keyPrefix = 'wmg:';
export const storageKeyItems = 'Items';
export const storageKeySettings = 'settings';

export async function loadItems() {
	if (getToken()) {
		let rItems = [];
		try { // TODO: move this try/catch to load
			rItems = await load();
		} catch(e) {
		}
		if (rItems && rItems.length) {
				saveToStorage(storageKeyItems, JSON.stringify(rItems));
				return rItems;
		}
	}
	return JSON.parse(loadFromStorage(storageKeyItems, '[]'));
}

export async function saveItems(items) {
	if (!items) { return; }
	if (getToken()) {
		await save(items);
	}
	saveToStorage(storageKeyItems, JSON.stringify(items));
}

export async function loadSettings(defaults) {
	if (getToken()) {
		let settings = null;
		try { 
			settings = await load(storageKeySettings);
			console.log('from server', settings, settings.length)
		} catch(e) {
		}
		if (settings) {
			saveToStorage(storageKeySettings, JSON.stringify(settings));
		}
	}
	return JSON.parse(loadFromStorage(storageKeySettings, JSON.stringify(defaults)));
}

export async function saveSettings(settings) {
	if (!settings) { return; }
	if (getToken()) {
		await save(settings, storageKeySettings);
	}
	saveToStorage(storageKeySettings, JSON.stringify(settings));
}


export async function purgeStorage(keys) {
	if (!supportsLocalStorage()) { return; }
	if (!keys || !keys.length || keys.includes(storageKeyItems)) {
		if (getToken()) {
			await save([]);
		}
	}
	if (!keys || !keys.length) {
		localStorage.clear();
	} else {
		for (const key of keys) {
			delete localStorage[key];
		}
	}

}

function saveToStorage(key, text) {
	if (!supportsLocalStorage()) { return; }

	try {
		localStorage.setItem(keyPrefix + key, text);
	} catch(e) {
		console.error('ERROR save to localStorage failed:', e);
	}
}

function loadFromStorage(key, defaultValue = '') {
	if (!supportsLocalStorage()) { return; }

	try {
		return localStorage.getItem(keyPrefix + key) || defaultValue;
	} catch (e) {
		console.error('ERROR in getKey - key not set', e);
		return defaultValue;
	}

}

function supportsLocalStorage() {
	if (!localStorage) {
		console.warn('localStorage is not supported by your browser.');
		return false;
	}
	return true;
}

