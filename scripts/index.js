'use strict';

import { CheckboxWithLabel, getConfirmation, SpDialog, ListItem } from './components/index.js';
import { SelectableItem, mergeStored } from './item.js';
import { itemList } from './item-list.js';
import { loadItems, loadSettings, saveItems, saveSettings, purgeStorage } from './storage.js';
import { DetailsAnimation } from './details-animation.js';
import { randomFromArray } from './randomize.js';

customElements.define('checkbox-with-label', CheckboxWithLabel);
customElements.define('list-item', ListItem);

const supportsDialog = typeof HTMLDialogElement === 'function';

if (supportsDialog) {
	customElements.define('sp-dialog', SpDialog);
}

document.addEventListener("DOMContentLoaded", async function initialize() {
	let deferSave = true;
	const stored = await loadItems();
	const items = mergeStored(itemList, stored);

	const defaultSettings = {
		hideUnselected: false,
	};

	let settings = await loadSettings(defaultSettings);

	const listElement = document.querySelector('#the-list');
	const chooseTrackButton = document.querySelector('#choose-track');
	const chooseTrackButtonMessage = document.querySelector('#choose-track-message');
	const itemCountElement = document.querySelector('#selected-Item-count');
	const hideUnselectedCheckbox = document.querySelector('#hide-unselected');
	const resultElement = document.querySelector('#result');
	const resetButton = document.querySelector('#reset-selections');

	const tiles = [];

	resultElement.innerHTML = '';

	const emptyTile = insertTileFromItem(SelectableItem('-- nothing chosen --'), resultElement, true);
	emptyTile.showcase = true;

	setCheckBoxesFromSettings();
	generateTiles();
	updateItemCount();
	toggleHideUnselected();

	document.querySelectorAll('details').forEach((object) => {
		new DetailsAnimation(object, 'summary', '.settings-content');
		const propKey = `${object.id}Expanded`;
		object.addEventListener('toggle', (e) => {
			settings[propKey] = e.currentTarget.open;
			saveSettingsUnlessDeferred();
		});
		object.open = settings[propKey];
	});

	setTimeout(() => deferSave = false, 0);

	hideUnselectedCheckbox.addEventListener('change', (e) => {
		settings.hideUnselected = e.currentTarget.checked;
		saveSettingsUnlessDeferred();
		toggleHideUnselected();
	})

	resetButton.addEventListener('click', async () => {
		if (supportsDialog) {
			getConfirmation('Reset all checkboxes',
				async () => {
					await purgeStorage();
					clearTiles();
					generateTiles();
					resetSettings();
					updateItemCount();
				});
		} else {
			await purgeStorage();
			window.location.reload();
		}
	});

	chooseTrackButton.addEventListener('click', () => {
		const originalText = chooseTrackButton.textContent;
		chooseTrackButtonMessage.textContent = 'Choosing...';
		chooseTrackButton.disabled = true;

		const chosenItem = { ...randomFromArray(items.filter((Item) => Item.isSelected)) };

		setTimeout(() => {
			if (!isEmpty(chosenItem)) {
				resultElement.innerHTML = '';
				const tile = insertTileFromItem(chosenItem, resultElement, false);
				tile.showcase = true;
				tile.addEventListener('change', (e) => {
					for (const item of tiles) {
						if (item.prompt === tile.prompt) {
							item.checked = tile.checked
						}
					}
				})
			} else {
				resultElement.innerHTML = '';
				const tile = insertTileFromItem(SelectableItem('nothing to choose from!'), resultElement, true);
				tile.showcase = true;
			}
			chooseTrackButtonMessage.textContent = originalText;
			chooseTrackButton.disabled = false;
			chooseTrackButton.focus();
		}, 300);

		function isEmpty(obj) { return Object.keys(obj).length == 0; }
	});

	function insertTileFromItem(prompt, parent, isDisabled = false) {
		const tile = document.createElement('list-item');
		tile.prompt = prompt.name;
		tile.checked = prompt.isSelected;
		tile.disabled = isDisabled;

		tile.addEventListener('change', async (e) => {
			prompt.isSelected = e.currentTarget.checked;
			updateItemCount();
			saveItemsUnlessDeferred()
		});

		parent.appendChild(tile);
		return tile;
	}

	function clearTiles() {
		tiles.splice(0, tiles.length);
		items.forEach(Item => Item.isSelected = true);
		listElement.innerHTML = '';
	}

	function fromStorageOrDefault(prop, defaultValue) {
		if (!prop) { return defaultValue };
		return settings.hasOwnProperty(prop) ? settings[prop] : defaultValue;
	}

	async function filterItems() {
		const oldDefer = deferSave;
		deferSave = true; // prevent saving for each checkbox change
		updateItemCount();
		if (!oldDefer) {
			saveItems(items);
		}
		deferSave = oldDefer;
}

	function generateTiles() {
		for (const Item of items) {
			const tile = insertTileFromItem(Item, listElement, false);
			tiles.push(tile);
		}
	}

	function setCheckBoxesFromSettings() {
		hideUnselectedCheckbox.checked = fromStorageOrDefault('hideUnselected', defaultSettings.hideUnselected);
		document.querySelectorAll('.pack-checkbox').forEach((checkbox) => {
			const pack = checkbox.getAttribute('data-packname') || '';
			checkbox.checked = fromStorageOrDefault(pack, true);
		});
		document.querySelectorAll('details').forEach((object) => {
			object.open = settings[`${object.id}Expanded`];
			object.querySelector('.settings-content').inert = !object.open;
		});
	}

	function resetSettings() {
		settings = { ...defaultSettings };
		settings.itemsExpanded = false;
		document.querySelectorAll('.pack-checkbox').forEach((checkbox) => {
			const pack = checkbox.getAttribute('data-packname') || '';
			settings[pack] = true;
		});
		saveSettingsUnlessDeferred();
		setCheckBoxesFromSettings();
	}

	function toggleHideUnselected() {
		listElement.classList.toggle('is-hiding-unselected', hideUnselectedCheckbox.checked);
	}

	function updateItemCount() {
		const selectedCount = items.filter((item) => item.isSelected).length;
		itemCountElement.innerHTML = `(${selectedCount}/${items.length})`;
	}

	function saveItemsUnlessDeferred() {
		if (!deferSave) {
			saveItems(items);
		}
	}	

	function saveSettingsUnlessDeferred() {
		if (!deferSave) {
			saveSettings(settings);
		}
	}
});